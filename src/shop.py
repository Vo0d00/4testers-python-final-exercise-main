class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    # dodawanie produkt do listy  produktów
    def add_product(self, product):
        self.products.append(product)

    # zwraca sumę cen wszystkich produktów w zamówieniu
    # += można zastąpić  dłuższą formą   total_price = total_price + product.get_price()
    def get_total_price(self):
        total_price = 0
        for product in self.products:
            total_price += product.get_price()
        return total_price

    # zwraca sumę ilości wszystkich produktów w zamówieniu
    # += dłuższa forma total_quantity = total_quantity + product.quantity
    def get_total_quantity_of_products(self):
        total_quantity = 0
        for product in self.products:
            total_quantity += product.quantity
        return total_quantity

    # oznacza zamówienie jako   zrealizowane
    def purchase(self):
        self.purchased = True


class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    # zwraca cenę produktu  ilość * cena jednostkowa
    def get_price(self):
        return self.unit_price * self.quantity
